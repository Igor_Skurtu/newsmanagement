package by.epam.newsman.service;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.mockito.Mockito.*;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagement.dao.impl.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.impl.Service;

@RunWith(MockitoJUnitRunner.class)
public class ServiceTest {

   @Mock
   private RoleDAO roleDAO;
    
   @InjectMocks
   private Service service;

   @Test
   public void roleServiceTest() throws ServiceException, DAOException {
	   
	   when(roleDAO.create(any(Role.class) )).thenReturn(1L);
	   
	   Role role = new Role();
	   service.addRole(role);
   }

}
