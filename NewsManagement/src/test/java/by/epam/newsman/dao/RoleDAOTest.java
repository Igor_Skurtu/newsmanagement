package by.epam.newsman.dao;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagement.dao.impl.RoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;

@ContextConfiguration(locations="classpath:springTestContext.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class RoleDAOTest {
    
	private static EmbeddedDatabase db;
	
	@Autowired(required=true)
    private DataSource dataSource;
    
    @Autowired(required=true)
    private RoleDAO roleDAO;
    
    @BeforeClass
    public static void setUp() throws Exception {
    	db = new EmbeddedDatabaseBuilder().addDefaultScripts().build();
    }
    
    @Test
    public void test1() throws DAOException {
    	/*
		Role role = new Role();
		
		role.setRoleName("tempRole");
		
    	System.out.println(roleDAO.create(role));
		*/
    }
    @Test
    public void test2() throws DAOException {
    	/*
    	System.out.println(roleDAO.read(1L).getRoleName());
    	*/
    }
    

}