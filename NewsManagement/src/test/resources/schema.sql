
drop table USERS if exists;
drop table ROLES if exists;
drop table NEWS_TAG if exists;
drop table NEWS_AUTHOR if exists;
drop table COMMENTS if exists;
drop table NEWS if exists;
drop table TAGS if exists;
drop table AUTHORS if exists;



create table ROLES (
ROLE_ID integer identity primary key, 
ROLE_NAME varchar(50)  not null
);

 
create table USERS (
USER_ID integer identity primary key, 
USER_NAME varchar(50) not null,
LOGIN varchar(30) not null,
PASSWORD varchar(30) not null,
ROLES_ROLE_ID integer not null,
FOREIGN KEY (ROLES_ROLE_ID) REFERENCES ROLES(ROLE_ID)
);

create table AUTHORS (
AUTHOR_ID integer identity primary key,
AUTHOR_NAME varchar(30) not null,
EXPIRED timestamp not null
);

create table TAGS (
TAG_ID integer identity primary key,
TAG_NAME varchar(30) not null
);

create table NEWS (
NEWS_ID integer identity primary key,
TITLE varchar(30) not null,
SHORT_TEXT varchar(100) not null,
FULL_TEXT varchar(2000) not null,
CREATION_DATE timestamp not null,
MODIFICATION_DATE date not null
);

create table COMMENTS (
COMMENT_ID integer identity primary key,
COMMENT_TEXT varchar(100) not null,
CREATION_DATE timestamp not null,
NEWS_NEWS_ID integer not null,
FOREIGN KEY (NEWS_NEWS_ID) REFERENCES NEWS(NEWS_ID)
);

create table NEWS_AUTHOR (
NEWS_NEWS_ID integer not null,
FOREIGN KEY (NEWS_NEWS_ID) REFERENCES NEWS(NEWS_ID),
AUTHORS_AUTHOR_ID integer not null,
FOREIGN KEY (AUTHORS_AUTHOR_ID) REFERENCES AUTHORS(AUTHOR_ID)
);

create table NEWS_TAG (
NEWS_NEWS_ID integer not null,
FOREIGN KEY (NEWS_NEWS_ID) REFERENCES NEWS(NEWS_ID),
TAGS_TAG_ID integer not null,
FOREIGN KEY (TAGS_TAG_ID) REFERENCES TAGS(TAG_ID)
);
