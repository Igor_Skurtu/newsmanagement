package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.impl.*;
import com.epam.newsmanagement.entity.*;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * @author Ihar_Skurtu
 *
 */

public class Service {
	
	@Autowired
	private RoleDAO roleDAO;
	
	public void addNews(News news, List<Long> authorIds, List<Long> tagIds) throws ServiceException {
		
	}
	
	public void editNews(News news, List<Long> authorIds, List<Long> tagIds) throws ServiceException {
		
	}
	
	public Long countAllNews() throws ServiceException {
		
		return 0L;
	}

	public void deleteComments(List<Long> commentIds) throws ServiceException {
		
	}

	public void addAuthorsToNews(List<Author> authors, Long newsId) throws ServiceException {
		
	}
	
	public void addCommentsToNews(List<Comment> comments, Long newsId) throws ServiceException {
		
	}
	
	public void addTagsToNews(List<Tag> tags, Long newsId) throws ServiceException {
		
	}
	
	public void deleteNews(Long newsId) throws ServiceException {
		
		// удаление тэгов новости
		//удаление комментов к новости
		//удаление авторов к новости (только из junction table)
	}
	
	
	
	
	
	
	
	
	public void addUser(User user) throws ServiceException {

		/*	

		try {
			userDAO.create(user);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in Service.addUser() ", e);
		}
		*/
	}

	public void addRole(Role role) throws ServiceException {

		try {
			roleDAO.create(role);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in Service.addRole() ", e);
		}
	}

	public Role getRoleById(long roleId) throws ServiceException {

		try {
			Role role = roleDAO.read(roleId);
			return role;
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in Service.getRoleById() ", e);
		}
	}

	public void updateRole(Role role, long id) throws ServiceException {

		RoleDAO roleDAO = new RoleDAO();


		try {
			roleDAO.update(role, id);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in Service.updateRole() ", e);
		}
	}

	public void deleteRole(long id) throws ServiceException {

		RoleDAO roleDAO = new RoleDAO();


		try {
			roleDAO.delete(id);
		} catch (DAOException e) {
			throw new ServiceException("ServiceException in Service.deleteRole() ", e);
		}
	}

}
