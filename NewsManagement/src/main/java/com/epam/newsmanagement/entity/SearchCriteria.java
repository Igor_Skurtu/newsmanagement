package com.epam.newsmanagement.entity;

import java.util.ArrayList;

public class SearchCriteria {
	
	ArrayList<Long> tagList;
	ArrayList<Long> authorsList;
	
	public ArrayList<Long> getTagList() {
		return tagList;
	}
	public void setTagList(ArrayList<Long> tagList) {
		this.tagList = tagList;
	}
	public ArrayList<Long> getAuthorsList() {
		return authorsList;
	}
	public void setAuthorsList(ArrayList<Long> authorsList) {
		this.authorsList = authorsList;
	}
	

}
