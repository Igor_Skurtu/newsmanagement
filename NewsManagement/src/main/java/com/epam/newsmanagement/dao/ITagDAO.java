package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;

public interface ITagDAO extends IDAO<Tag> {

}
