package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;

public interface IUserDAO extends IDAO<User> {

}
