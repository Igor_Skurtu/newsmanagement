package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

public class TagDAO implements ITagDAO {
	
	private final String CREATE_TAG = "INSERT INTO TAGS (TAG_NAME) VALUES (?)";
	private final String READ_TAG = "SELECT TAG_ID, TAG_NAME FROM TAGS WHERE TAG_ID=?";
	private final String READ_ALL_TAGS = "SELECT TAG_ID, TAG_NAME FROM TAGS";
	private final String READ_ALL_TAGS_IDS = "SELECT TAGS_TAG_ID FROM NEWS_TAG WHERE NEWS_NEWS_ID=?";
	private final String UPDATE_TAG = "UPDATE TAGS SET TAG_NAME=? WHERE TAG_ID=?";
	private final String DELETE_TAG_FROM_NEWS_TAG = "DELETE FROM NEWS_TAG WHERE TAGS_TAG_ID=?";
	private final String DELETE_TAG = "DELETE FROM TAGS WHERE TAG_ID=?";

	@Autowired
	private DataSource dataSource;

	public long create(Tag t) throws DAOException {
		Connection connection = null;
		String[] column_names = { "TAG_ID" }; // for Oracle database to prevent empty
													// ResultSet from
													// getGeneratedKeys()
		Long tagId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_TAG, column_names);

			preparedStatement.setString(1, t.getTagName());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				tagId = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.create()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return tagId;
	}

	public Tag read(long tagId) throws DAOException {
		Tag tag = new Tag();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_TAG);

			preparedStatement.setLong(1, tagId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.read()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return tag;
	}
	
	public List<Tag> readAll() throws DAOException {
		List<Tag> tagsList = new ArrayList<Tag>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_TAGS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));
				tagsList.add(tag);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.readAll()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return tagsList;
	}
	
	public List<Tag> readAllByNewsId(long newsId) throws DAOException {
		List<Tag> tagsList = new ArrayList<Tag>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_TAGS_IDS);
			preparedStatement.setLong(1, newsId);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			preparedStatement = connection.prepareStatement(READ_TAG);

			while (resultSet.next()) {

				preparedStatement.setLong(1, resultSet.getLong(1));
				preparedStatement.addBatch();
			}

			preparedStatement.executeBatch();
			resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Tag tag = new Tag();
				tag.setTagId(resultSet.getLong(1));
				tag.setTagName(resultSet.getString(2));
				tagsList.add(tag);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.readAllByNewsId()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return tagsList;
	}

	public void update(Tag t, long tagId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_TAG);

			preparedStatement.setString(1, t.getTagName());
			preparedStatement.setLong(2, tagId);

			preparedStatement.executeUpdate();

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TagDAO.update()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	public void delete(long tagId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_TAG_FROM_NEWS_TAG);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
			
			preparedStatement = connection.prepareStatement(DELETE_TAG);
			preparedStatement.setLong(1, tagId);
			preparedStatement.executeUpdate();
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in TAgDAO.delete()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}


}
