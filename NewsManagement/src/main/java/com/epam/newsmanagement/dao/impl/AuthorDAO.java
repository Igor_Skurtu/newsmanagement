package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

public class AuthorDAO implements IAuthorDAO {

	private final String CREATE_AUTHOR = "INSERT INTO AUTHORS (AUTHOR_NAME, EXPIRED) VALUES (?, ?)";
	private final String READ_AUTHOR = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS WHERE AUTHOR_ID=?";
	private final String READ_ALL_AUTHORS = "SELECT AUTHOR_ID, AUTHOR_NAME, EXPIRED FROM AUTHORS";
	private final String READ_ALL_AUTHORS_IDS = "SELECT AUTHORS_AUTHOR_ID FROM NEWS_AUTHOR WHERE NEWS_NEWS_ID=?";
	private final String UPDATE_AUTHOR = "UPDATE AUTHORS SET AUTHOR_NAME=?, EXPIRED=? WHERE AUTHOR_ID=?";

	@Autowired
	private DataSource dataSource;

	public long create(Author t) throws DAOException {
		Connection connection = null;
		String[] column_names = { "AUTHOR_ID" }; // for Oracle database to
													// prevent
													// empty
													// ResultSet from
													// getGeneratedKeys()
		Long authorId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_AUTHOR, column_names);

			preparedStatement.setString(1, t.getAuthorName());
			preparedStatement.setTimestamp(2, t.getExpired());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				authorId = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.create()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return authorId;
	}

	public Author read(long authorId) throws DAOException {
		Author author = new Author();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_AUTHOR);

			preparedStatement.setLong(1, authorId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.read()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return author;
	}

	public List<Author> readAll() throws DAOException {
		List<Author> authorsList = new ArrayList<Author>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_AUTHORS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Author author = new Author();
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));
				authorsList.add(author);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.readAll()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return authorsList;
	}

	public List<Author> readAllByNewsId(long newsId) throws DAOException {
		List<Author> authorsList = new ArrayList<Author>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_AUTHORS_IDS);
			preparedStatement.setLong(1, newsId);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			preparedStatement = connection.prepareStatement(READ_AUTHOR);

			while (resultSet.next()) {

				preparedStatement.setLong(1, resultSet.getLong(1));
				preparedStatement.addBatch();
			}

			preparedStatement.executeBatch();
			resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Author author = new Author();
				author.setAuthorId(resultSet.getLong(1));
				author.setAuthorName(resultSet.getString(2));
				author.setExpired(resultSet.getTimestamp(3));
				authorsList.add(author);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.readAllByNewsId()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return authorsList;
	}

	public void update(Author t, long authorId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_AUTHOR);

			preparedStatement.setString(1, t.getAuthorName());
			preparedStatement.setTimestamp(2, t.getExpired());
			preparedStatement.setLong(3, authorId);

			preparedStatement.executeUpdate();

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in AuthorDAO.update()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

	}

	public void delete(long authorId) throws DAOException {

	}

}
