package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

public class CommentDAO implements ICommentDAO {
	
	private final String CREATE_COMMENT = "INSERT INTO COMMENTS (NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE) VALUES (?, ?, ?)";
	private final String READ_COMMENT = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE COMMENT_ID=?";
	private final String READ_ALL_COMMENTS = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS";
	private final String READ_ALL_NEWS_COMMENTS = "SELECT COMMENT_ID, NEWS_NEWS_ID, COMMENT_TEXT, CREATION_DATE FROM COMMENTS WHERE NEWS_NEWS_ID=?";
	private final String UPDATE_COMMENT = "UPDATE COMMENTS SET NEWS_NEWS_ID=?, COMMENT_TEXT=?, CREATION_DATE=? WHERE COMMENT_ID=?";
	private final String DELETE_COMMENT = "DELETE FROM COMMENTS WHERE COMMENT_ID=?";
	private final String DELETE_COMMENT_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE NEWS_NEWS_ID=?";

	@Autowired
	private DataSource dataSource;

	public long create(Comment t) throws DAOException {
		Connection connection = null;
		String[] column_names = { "COMMENT_ID" }; // for Oracle database to prevent empty
													// ResultSet from
													// getGeneratedKeys()
		Long commentId = null;
		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(CREATE_COMMENT, column_names);

			preparedStatement.setLong(1, t.getNewsID());
			preparedStatement.setString(2, t.getCommentText());
			preparedStatement.setTimestamp(3, t.getCreationDate());
			preparedStatement.executeUpdate();
			ResultSet resultSet = preparedStatement.getGeneratedKeys();

			if (resultSet.next()) {
				commentId = resultSet.getLong(1);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.create()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return commentId;
	}

	public Comment read(long commentId) throws DAOException {
		Comment comment = new Comment();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_COMMENT);

			preparedStatement.setLong(1, commentId);
			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			if (resultSet.next()) {
				comment.setNewsID(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.read()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return comment;
	}
	
	public List<Comment> readAll() throws DAOException {
		List<Comment> commentsList = new ArrayList<Comment>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_COMMENTS);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setNewsID(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				commentsList.add(comment);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.readAll()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return commentsList;
	}
	
	public List<Comment> readAllByNewsId(long newsId) throws DAOException {
		List<Comment> commentsList = new ArrayList<Comment>();

		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(READ_ALL_NEWS_COMMENTS);
			preparedStatement.setLong(1, newsId);

			preparedStatement.execute();
			ResultSet resultSet = preparedStatement.getResultSet();

			while (resultSet.next()) {
				Comment comment = new Comment();
				comment.setNewsID(resultSet.getLong(1));
				comment.setCommentText(resultSet.getString(2));
				comment.setCreationDate(resultSet.getTimestamp(3));
				commentsList.add(comment);
			}

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.readAllByNewsId()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}

		return commentsList;
	}

	public void update(Comment t, long commentId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_COMMENT);

			preparedStatement.setLong(1, t.getNewsID());
			preparedStatement.setString(2, t.getCommentText());
			preparedStatement.setTimestamp(3, t.getCreationDate());
			preparedStatement.setLong(4, commentId);

			preparedStatement.executeUpdate();

			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.update()", e);

		} finally {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}

	public void delete(long commentId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMMENT);
			preparedStatement.setLong(1, commentId);
			preparedStatement.executeUpdate();
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.delete()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}
	
	public void deleteAllByNewsId(long newsId) throws DAOException {
		Connection connection = null;

		try {
			connection = DataSourceUtils.getConnection(dataSource);
			PreparedStatement preparedStatement = connection.prepareStatement(DELETE_COMMENT_BY_NEWS_ID);
			preparedStatement.setLong(1, newsId);
			preparedStatement.executeUpdate();
			
			preparedStatement.close();

		} catch (SQLException e) {
			throw new DAOException("SQLException in CommentDAO.deleteAllByNewsId()", e);

		} finally {
			 DataSourceUtils.releaseConnection(connection, dataSource);
		}
	}



}
