package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;

public interface IAuthorDAO extends IDAO<Author> {

}
